﻿#include <iostream>

bool Success = false;

class Fraction {
private:
	float numerator = 0;
	float denominator = 0;
public:
	Fraction(float Numerator, float Denominator) {
		try {
			if (Denominator == 0) {
				throw std::runtime_error("Делить на ноль нельзя!");
			}

			this->numerator = Numerator;
			this->denominator = Denominator;

			std::cout << "Результат: " << numerator / denominator << std::endl;
			Success = true;

		}
		catch (std::exception except) {
			std::cout << except.what() << std::endl << std::endl;
		}
	}
};


int main() 
{
	setlocale(LC_ALL, "Russian");

	float Numerator = 0;
	float Denominator = 0;

	while (!Success) {
		std::cout << "Введите делимое: ";
		std::cin >> Numerator;
		if (std::cin.fail() || std::cin.peek() != '\n')
		{
			std::cin.clear();
			std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
			std::cout << "Введены некорректные данные!" << std::endl << std::endl;
			continue;
		}
		std::cout << "Введите делитель: ";
		std::cin >> Denominator;
		if (std::cin.fail() || std::cin.peek() != '\n')
		{
			std::cin.clear();
			std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
			std::cout << "Введены некорректные данные!" << std::endl << std::endl;
			continue;
		}

		Fraction Temp(Numerator, Denominator);
	}
	return 0;
}